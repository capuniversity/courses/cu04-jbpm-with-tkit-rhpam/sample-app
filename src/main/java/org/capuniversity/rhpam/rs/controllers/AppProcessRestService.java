package org.capuniversity.rhpam.rs.controllers;

import lombok.extern.slf4j.Slf4j;
import org.tkit.jee.base.exception.ServiceException;
import org.tkit.rhpam.client.wih.services.ProcessService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Path("process")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class AppProcessRestService {

    @EJB
    private ProcessService processService;

    @GET
    public String ping() {
        return "PONG";
    }

    @POST
    @Path("start/{deploymentId}/{processId}")
    public void start(@PathParam("deploymentId") String deploymentId, @PathParam("processId") String processId, Map<String, Object> data) throws ServiceException {
        String processLogGuid = processService.startProcess(deploymentId, processId, 100L, "DomainEntityKey", data);
        log.info("Start the process. Process log: {}", processLogGuid);
    }

    @GET
    @Path("sendMessage/{deploymentId}/{processInstanceId}/{messageId}")
    public void message(@PathParam("deploymentId") String deploymentId, @PathParam("processInstanceId") Long processInstanceId, @PathParam("messageId") String messageId) throws ServiceException {
        String data = UUID.randomUUID().toString();
        processService.sendMessage(deploymentId, processInstanceId, messageId, data);
        log.info("Send message to {} {} {} {}", deploymentId, processInstanceId, messageId, data);
    }
}
