package org.tkit.rhpam.example.app.rs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class JaxrsApplication extends Application {
}
