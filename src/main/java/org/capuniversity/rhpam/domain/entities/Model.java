package org.capuniversity.rhpam.domain.entities;

import lombok.Getter;
import lombok.Setter;
import org.tkit.jee.jpa.model.entity.PersistentStringGuid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "TB_MODEL")
public class Model extends PersistentStringGuid {

    @Column(name = "NAME")
    private String name;
}
