package org.capuniversity.rhpam.process.mdb;

import lombok.extern.slf4j.Slf4j;
import org.capuniversity.rhpam.process.services.DemoProcessLogicService;
import org.tkit.jee.base.exception.ServiceException;
import org.tkit.rhpam.client.wih.jms.AbstractProcessClientService;
import org.tkit.rhpam.client.wih.model.ProcessWorkItem;
import org.tkit.rhpam.client.wih.model.WorkItemResult;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;

/**
 * This class will receive all messages from tkit-rhpam/process engine.
 * It is a MDB - message driven bean, so the server will invoke it whenever a new message is ready in the queue defined
 * with the `destinationLookup` property
 */
@Slf4j
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        //name of the queue, that we process = 'queues/' + ID_OF_OUR_PROCESS
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "queues/process.DemoProcess"),
        //for processes we always use queues(topics would be for broadcast messages)
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
//we extend the AbstractProcessClientService from tkit-rhpam. Technically the JMS message will be read by the abstract class(onMessage method) and
//then the abstract class invokes our doExecute method with the parsed process info
public class MoreInterestingProcessService extends AbstractProcessClientService {
    //We can inject any business component we want
    @Inject
    private DemoProcessLogicService demoProcessLogicService;

    @Override
    protected WorkItemResult doExecute(ProcessWorkItem processWorkItem) throws ServiceException {
        log.info("Processing message for process: {}\n\tstep: {}\n\tnode type {}\n\tparams: {}\n\tprocess step log id: {}",
                processWorkItem.getProcessId(), processWorkItem.getName(), processWorkItem.getNodeType(), processWorkItem.getParameters(),
                processWorkItem.getProcessStepLogGuid());

        // for bigger processes it makes sense to split the logic into dedicated services
        String stepName = processWorkItem.getName();
        switch (stepName) {
            case "BPS_FIRST_STEP":
                return demoProcessLogicService.doStep1(processWorkItem);
            case "BPS_CHECK_TIME":
                return demoProcessLogicService.doStep2(processWorkItem);
            case "BPS_CONDITIONAL_STEP":
                return demoProcessLogicService.doStep3(processWorkItem);
            default:
                throw new IllegalArgumentException("Unknown step name: " + stepName);

        }
    }
}
