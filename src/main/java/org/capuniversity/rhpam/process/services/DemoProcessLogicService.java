package org.capuniversity.rhpam.process.services;

import lombok.extern.slf4j.Slf4j;
import org.tkit.jee.base.exception.ServiceException;
import org.tkit.rhpam.client.processlog.model.Message;
import org.tkit.rhpam.client.processlog.model.dao.MessageDAO;
import org.tkit.rhpam.client.processlog.model.enums.MessageType;
import org.tkit.rhpam.client.wih.factories.MessageFactory;
import org.tkit.rhpam.client.wih.model.ProcessWorkItem;
import org.tkit.rhpam.client.wih.model.ResolutionStatus;
import org.tkit.rhpam.client.wih.model.WorkItemResult;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalField;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
@Slf4j
@ApplicationScoped
public class DemoProcessLogicService {

    public static final String IMPORTANT_PARAM_NAME = "step1output";
    public static final String IS_MINUTE_ODD_PARAM_NAME = "isminuteodd";


    @Inject
    MessageDAO msgDao;

    public WorkItemResult doStep1(ProcessWorkItem pwi) {
        //we have to return an instance of WorkItemResult that has 2 most commonly used attributes:
        //status: that indicates if our step was executed successfully or not
        //data: any output params, that we want to pass to the process engine(e.g. to next step)
        Map<String, Object> outputParams = new HashMap<>();
        outputParams.put(IMPORTANT_PARAM_NAME, "abc_" + System.currentTimeMillis());
        return WorkItemResult.builder().data(outputParams).status(ResolutionStatus.SUCCESSFUL).build();
    }

    public WorkItemResult doStep2(ProcessWorkItem pwi) throws ServiceException {
        //if the time is odd number, throw error
        if (System.currentTimeMillis() % 2 > 0) {
            //we can create process log messages at any time and attach them to process log
            //in this case the msg is attache to the process step log from `pwi.getProcessStepLogGuid()`
            msgDao.create(MessageFactory.createMessage(pwi, "Timewarping error", MessageType.ERROR));
            throw new ServiceException(ProcessErrors.TIME_IS_NOT_RIGHT);
        }
        //to access input params of this step, simply ask the workitem
        String paramFromStep1 = pwi.getParameter(IMPORTANT_PARAM_NAME);

        //we can also create process log messages for any relevant business data
        //in this case the msg is attache to the process step log from `pwi.getProcessStepLogGuid()`
        Message importantBusinessMessage = MessageFactory.createMessage(pwi, "Timewarp OK", MessageType.INFO);
        //message has a bunch of attributes where you can store more info, including big data: like err dumps from remote calls
        importantBusinessMessage.setDescription("Step 1 gave us this: " + paramFromStep1);
        msgDao.create(importantBusinessMessage);
        //we have to return an instance of WorkItemResult that has 2 most commonly used attributes:
        //status: that indicates if our step was executed successfully or not
        //data: any output params, that we want to pass to the process engine(e.g. to next step)
        Map<String, Object> outputParams = new HashMap<>();
        int minute = LocalTime.now().get(ChronoField.MINUTE_OF_HOUR);
        outputParams.put(IS_MINUTE_ODD_PARAM_NAME, minute % 2 == 0 );
        return WorkItemResult.builder().data(outputParams).status(ResolutionStatus.SUCCESSFUL).build();
    }

    public WorkItemResult doStep3(ProcessWorkItem pwi) {
        return WorkItemResult.builder().status(ResolutionStatus.SUCCESSFUL).build();
    }

    enum ProcessErrors {
        TIME_IS_NOT_RIGHT
    }
}
