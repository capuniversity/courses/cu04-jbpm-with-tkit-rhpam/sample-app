#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER rhpam WITH ENCRYPTED PASSWORD 'rhpam';
    CREATE DATABASE rhpam;
    GRANT ALL PRIVILEGES ON DATABASE rhpam TO rhpam;
	
    CREATE USER rhpam_example_app WITH ENCRYPTED PASSWORD 'rhpam_example_app';
    CREATE DATABASE rhpam_example_app;
    GRANT ALL PRIVILEGES ON DATABASE rhpam_example_app TO rhpam_example_app;

    CREATE USER diff_db WITH ENCRYPTED PASSWORD 'diff_db';
    CREATE DATABASE diff_db;
    GRANT ALL PRIVILEGES ON DATABASE diff_db TO diff_db;
EOSQL
