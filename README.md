# sample-app

Sample BPM client application

## Build & setup 

The project relies on some libraries not available in the central maven repo. Therefore you need to use custom maven settings located in this project.
From command line you can build the project like this:  

`mvn clean package -s settings.xml`

## Run via docker compose

Build this project and `sample-process` first using :  

```
mvn clean package -s settings.xml -P docker
```

Then you can run all pieces together from this repo(please stop any other docker contains you might have running locally):

`docker-compose up -d`
Verify that there are no issues and that you have 4 containers running

## Starting a process instance

There is a rest endpoint src/main/java/org/capuniversity/rhpam/rs/controllers/AppProcessRestService.java that you can invoke form cmd:
`▶ curl -X POST -H "Content-Type: application/json" http://localhost:9080/process/start/sample-process/process.DemoProcess`